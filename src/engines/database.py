import logging
from sqlalchemy import create_engine, Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


class Database:
    def __init__(self, model, database_url, timeout=10, logger=None):
        """
        Initialize the Database object.

        Parameters:
        - model: The SQLAlchemy model to be used.
        - database_url: The URL of the database.
        - timeout: The timeout for the database connection pool (default is 10 seconds).
        - logger: Optional logger object for logging (default is None).
        """
        self.logger = logger or logging.getLogger(__name__)
        self.database_url = database_url
        self.engine = None
        self.Session = None
        self.model = model
        self.timeout = timeout

        try:
            self.engine = create_engine(database_url, pool_timeout=self.timeout)
            self.Session = sessionmaker(bind=self.engine)
            self.create_tables()
        except Exception as e:
            self.logger.error(f"Error during database initialization: {str(e)}")
            raise

    def create_tables(self):
        """
        Create database tables based on the specified model.
        """
        try:
            self.model.metadata.create_all(self.engine)
            self.logger.info("Database tables created.")
        except Exception as e:
            self.logger.error(f"Error creating database tables: {str(e)}")
            raise

    def add_entity(self, **kwargs):
        """
        Add a new entity to the database.

        Parameters:
        - kwargs: Keyword arguments representing the entity attributes.
        """
        try:
            entity = self.model(**kwargs)
            with self.Session() as session:
                session.add(entity)
                session.commit()
                self.logger.info(f"Entity added to the database: {entity}")
        except Exception as e:
            self.logger.error(f"Error adding entity to the database: {str(e)}")
            raise

    def get_all_entities(self):
        """
        Retrieve all entities from the database.

        Returns:
        A list of dictionaries, each representing an entity.
        """
        try:
            with self.Session() as session:
                entities = session.query(self.model).all()
                self.logger.info("All entities retrieved from the database.")
                return [self.model_to_dict(entity) for entity in entities]
        except Exception as e:
            self.logger.error(f"Error retrieving all entities: {str(e)}")
            raise
        
        
    def model_to_dict(self, model):
        """
        Convert a SQLAlchemy model object to a dictionary.

        Parameters:
        - model: The SQLAlchemy model object.

        Returns:
        A dictionary representing the model object.
        """
        return {column.name: getattr(model, column.name) for column in model.__table__.columns}