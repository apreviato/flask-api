import logging
from engines.database import Database
from models.asset_class import AssetClass


class AssetService:
    def __init__(self, configuration, logger=None) -> None:
        """
        Initialize the AssetService.

        Parameters:
        - configuration: The configuration object containing DATABASE_URI.
        - logger: Optional logger object for logging (default is None).
        """
        self.logger = logger or logging.getLogger(__name__)
        self.asset_db = Database(
            model=AssetClass,
            database_url=configuration.DATABASE_URI,
            timeout=10,
            logger=self.logger,
        )

    def get_all(self) -> list[AssetClass]:
        """
        Get all asset classes.

        Returns:
        A list of AssetClass objects.
        """
        self.logger.info("Attempting to retrieve all asset classes.")
        return [
            AssetClass(ID=1, Name="Test", Value=""),
            AssetClass(ID=1, Name="Test 1", Value=""),
        ]

    def get(self, id: str) -> AssetClass:
        """
        Get an asset class by ID.

        Parameters:
        - id: The ID of the asset class to retrieve.

        Returns:
        An AssetClass object.
        """
        self.logger.info(f"Attempting to retrieve asset class with ID: {id}")
        asset = AssetClass(ID=id, Name="Test", Value="")
        return asset

    def new(self, newClass: AssetClass) -> bool:
        """
        Create a new asset class.

        Parameters:
        - newClass: The AssetClass object representing the new asset class.

        Returns:
        True if the asset class is successfully created, False otherwise.
        """
        self.logger.info(f"Creating a new asset class: {newClass}")
        return True

    def update(self, newClass: AssetClass) -> bool:
        """
        Update an existing asset class.

        Parameters:
        - newClass: The AssetClass object representing the updated asset class.

        Returns:
        True if the asset class is successfully updated, False otherwise.
        """
        self.logger.info(f"Updating asset class: {newClass}")
        return True

    def remove(self, id: str) -> bool:
        """
        Remove an asset class by ID.

        Parameters:
        - id: The ID of the asset class to remove.

        Returns:
        True if the asset class is successfully removed, False otherwise.
        """
        self.logger.info(f"Removing asset class with ID: {id}")
        return True