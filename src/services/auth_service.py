import logging
from services.engines.database import Database
from models.user import User
from passlib.hash import pbkdf2_sha256


class AuthService:
    def __init__(self, configuration, logger=None) -> None:
        """
        Initialize the AuthService.

        Parameters:
        - configuration: The configuration object containing DATABASE_URI.
        - logger: Optional logger object for logging (default is None).
        """
        self.logger = logger or logging.getLogger(__name__)
        self.auth_db = Database(
            model=User,
            database_url=configuration.DATABASE_URI,
            timeout=10,
            logger=logger,
        )

    def login(self, username, password) -> str:
        """
        Perform user login.

        Parameters:
        - username: The username for login.
        - password: The password for login.

        Returns:
        A list of dictionaries containing user information.
        """
        all_users = self.auth_db.get_all_entities()
        self.logger.info("User login attempted.")
        return [
            {
                "name": user["name"],
                "profile": user["profile"],
            }
            for user in all_users
        ]

    def new(self, name, username, password) -> bool:
        """
        Create a new user.

        Parameters:
        - name: The name of the new user.
        - username: The username of the new user.
        - password: The password of the new user.

        Returns:
        True if the user is successfully created, False otherwise.
        """
        new_user = User(name, username, pbkdf2_sha256.hash(password))
        self.auth_db.add_entity(new_user)
        self.logger.info(f"New user created: {new_user}")
        return True
