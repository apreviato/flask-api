import logging
from services.engines.database import Database
from models.project import Project


class ProjectService:
    def __init__(self, configuration, logger=None) -> None:
        """
        Initialize the ProjectService.

        Parameters:
        - configuration: The configuration object containing DATABASE_URI.
        - logger: Optional logger object for logging (default is None).
        """
        self.logger = logger or logging.getLogger(__name__)
        self.project_db = Database(
            model=Project,
            database_url=configuration.DATABASE_URI,
            timeout=10,
            logger=self.logger,
        )

    def get_all(self) -> list[Project]:
        """
        Get all projects.

        Returns:
        A list of dictionaries containing project information.
        """
        with self.project_db.Session() as session:
            projects = session.query(Project).all()
            self.logger.info("Attempting to retrieve all projects.")
            return [
                {
                    "name": project.nome,
                    "classes": project.classes,
                }
                for project in projects
            ]

    def get(self, id: str) -> Project:
        """
        Get a project by ID.

        Parameters:
        - id: The ID of the project to retrieve.

        Returns:
        A Project object.
        """
        self.logger.info(f"Attempting to retrieve project with ID: {id}")
        project = Project(
            ID=id,
            Name="Test",
        )
        return project

    def new(self, newProject: Project) -> bool:
        """
        Create a new project.

        Parameters:
        - newProject: The Project object representing the new project.

        Returns:
        True if the project is successfully created, False otherwise.
        """
        self.logger.info(f"Creating a new project: {newProject}")
        return True

    def update(self, newProject: Project) -> bool:
        """
        Update an existing project.

        Parameters:
        - newProject: The Project object representing the updated project.

        Returns:
        True if the project is successfully updated, False otherwise.
        """
        self.logger.info(f"Updating project: {newProject}")
        return True

    def remove(self, id: str) -> bool:
        """
        Remove a project by ID.

        Parameters:
        - id: The ID of the project to remove.

        Returns:
        True if the project is successfully removed, False otherwise.
        """
        self.logger.info(f"Removing project with ID: {id}")
        return True