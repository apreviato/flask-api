import os
import sys
import logging
from flask import Flask
from werkzeug.utils import import_string
from controllers.auth_controller import AuthController, auth_bp
from controllers.asset_class_controller import AssetController, asset_class_bp
from controllers.project_controller import ProjectController, project_bp
from security.auth import initialize_jwt

app = Flask(__name__)

def load_configuration():
    app_config = os.environ.get("FLASK_ENV", "development")
    configuration = import_string(f"config.{app_config.capitalize()}Config")
    app.config.from_object(configuration)
    logging.basicConfig(filename=configuration.LOG_PATH, level=logging.DEBUG)

def register_routes():
    app.register_blueprint(auth_bp, url_prefix=app.config["BASE_URL"] + "/auth")
    app.register_blueprint(project_bp, url_prefix=app.config["BASE_URL"] + "/projects")
    app.register_blueprint(asset_class_bp, url_prefix=app.config["BASE_URL"] + "/assets")

def create_controllers():
    app.auth_controller = AuthController(app.config, logging)
    app.asset_controller = AssetController(app.config, logging)
    app.project_controller = ProjectController(app.config, logging)

def initialize_services():
    app.auth_controller.init_service()
    app.asset_controller.init_service()
    app.project_controller.init_service()

def configure_app():
    load_configuration()
    register_routes()
    create_controllers()
    initialize_jwt(app)

if __name__ == "__main__":
    try:
        app.before_request_funcs = [(None, initialize_services)]
        configure_app()
        app.run(debug=True)
    except Exception as e:
        logging.error(f"Error during app startup: {str(e)}")
        sys.exit(1)