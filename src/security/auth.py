from flask import request, jsonify
from flask_jwt_extended import JWTManager, jwt_required, get_jwt_identity, get_jwt
from models.user import Profile
jwt = JWTManager()


def initialize_jwt(app):
    jwt.init_app(app)


def login():
    pass


def auth(required_profile: Profile = Profile.BASE):
    """
    Decorator for route authentication.

    Parameters:
    - required_profile: The required user profile to access the route (default is None).

    Returns:
    A decorator function that checks for valid JWT authentication and user profile.
    """
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                jwt_required()(lambda: None)()

                current_user = get_jwt_identity()
                user_profile = get_jwt().get("profile")

                if user_profile and user_profile != required_profile:
                    return (
                        jsonify(
                            {"message": "Insufficient profile to access this route"}
                        ),
                        403,
                    )

                # BUG
                # kwargs['current_user'] = str(current_user)
                # kwargs['user_profile'] = str(user_profile)

                return func(*args, **kwargs)

            except Exception as e:
                print(e)
                return jsonify({"message": "Authentication failure"}), 401

        return wrapper

    return decorator