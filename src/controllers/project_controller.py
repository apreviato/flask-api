import logging
from flask import Blueprint, current_app, jsonify
from security.auth import auth
from models.project import Project
from models.user import Profile
from services.project_service import ProjectService

project_bp = Blueprint("project_bp", __name__)


class ProjectController:
    def __init__(self, configuration, logger=None) -> None:
        self.configuration = configuration
        self.logger = logger or logging.getLogger(__name__)
        
    def init_service(self) -> None:
        current_app.project_service = ProjectService(self.configuration, self.logger)
        
    @project_bp.route("/get_all", endpoint="/get_all", methods=["GET"])
    @auth(required_profile=Profile.BASE)
    def get_all():
        """
        Endpoint to retrieve all projects.

        Returns:
        JSON response containing the list of projects.
        """
        return jsonify(current_app.project_service.get_all())

    @project_bp.route("/get/<int:id>", endpoint="/get/<int:id>", methods=["GET"])
    @auth(required_profile=Profile.BASE)
    def get(id: str):
        """
        Endpoint to retrieve a project by ID.

        Parameters:
        - id: The ID of the project to retrieve.

        Returns:
        JSON response containing the project information.
        """
        return jsonify(current_app.project_service.get(id))

    @project_bp.route("/new", endpoint="/new", methods=["POST"])
    @auth(required_profile=Profile.BASE)
    def new(project: Project):
        """
        Endpoint to create a new project.

        Parameters:
        - project: The Project object representing the new project.

        Returns:
        JSON response indicating success or failure of the operation.
        """
        return jsonify(current_app.project_service.new(project))

    @project_bp.route("/update", endpoint="/update", methods=["POST"])
    @auth(required_profile=Profile.BASE)
    def update(project: Project):
        """
        Endpoint to update an existing project.

        Parameters:
        - project: The Project object representing the updated project.

        Returns:
        JSON response indicating success or failure of the operation.
        """
        return jsonify(current_app.project_service.update(project))

    @project_bp.route("/remove", endpoint="/remove", methods=["PUT"])
    @auth(required_profile=Profile.BASE)
    def remove(id: str):
        """
        Endpoint to remove a project by ID.

        Parameters:
        - id: The ID of the project to remove.

        Returns:
        JSON response indicating success or failure of the operation.
        """
        return jsonify(current_app.project_service.remove(id))
