import logging
from flask import Blueprint, current_app, jsonify
from security.auth import auth
from models.asset_class import AssetClass
from models.user import Profile
from services.asset_service import AssetService

asset_class_bp = Blueprint("asset_class_bp", __name__)


class AssetController:
    def __init__(self, configuration, logger=None) -> None:
        self.configuration = configuration
        self.logger = logger or logging.getLogger(__name__)
        
    def init_service(self) -> None:
        current_app.asset_class_service = AssetService(self.configuration, self.logger)
        
    @asset_class_bp.route("/get_all", endpoint="/get_all", methods=["GET"])
    @auth(required_profile=Profile.BASE)
    def get_all():
        """
        Get all asset classes.

        Returns:
        JSON response containing the list of asset classes.
        """
        return jsonify(current_app.asset_class_service.get_all())

    @asset_class_bp.route("/get/<int:id>", endpoint="/get/<int:id>", methods=["GET"])
    @auth(required_profile=Profile.BASE)
    def get(id: str):
        """
        Get an asset class by ID.

        Parameters:
        - id: The ID of the asset class to retrieve.

        Returns:
        JSON response containing the asset class information.
        """
        return jsonify(current_app.asset_class_service.get(id))

    @asset_class_bp.route("/new", endpoint="/new", methods=["POST"])
    @auth(required_profile=Profile.BASE)
    def new(asset: AssetClass):
        """
        Create a new asset class.

        Parameters:
        - asset: The AssetClass object representing the new asset class.

        Returns:
        JSON response indicating success or failure of the operation.
        """
        return jsonify(current_app.asset_class_service.new(asset))

    @asset_class_bp.route("/update", endpoint="/update", methods=["POST"])
    @auth(required_profile=Profile.BASE)
    def update(asset: AssetClass):
        """
        Update an existing asset class.

        Parameters:
        - asset: The AssetClass object representing the updated asset class.

        Returns:
        JSON response indicating success or failure of the operation.
        """
        return jsonify(current_app.asset_class_service.update(asset))

    @asset_class_bp.route("/remove", endpoint="/remove", methods=["PUT"])
    @auth(required_profile=Profile.BASE)
    def remove(id: str):
        """
        Remove an asset class by ID.

        Parameters:
        - id: The ID of the asset class to remove.

        Returns:
        JSON response indicating success or failure of the operation.
        """
        return jsonify(current_app.asset_class_service.remove(id))
