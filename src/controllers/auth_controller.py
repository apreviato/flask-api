import logging
from flask import Blueprint, current_app, jsonify, request
from flask_jwt_extended import create_access_token
from services.auth_service import AuthService

auth_bp = Blueprint("auth_bp", __name__)


class AuthController:
    def __init__(self, configuration, logger=None) -> None:
        self.configuration = configuration
        self.logger = logger or logging.getLogger(__name__)
        
    def init_service(self) -> None:
        self.logger = self.logger or logging.getLogger(__name__)
        current_app.auth_service = AuthService(self.configuration, self.logger)

    @auth_bp.route("/login", endpoint="/login", methods=["POST"])
    def login():
        """
        Endpoint for user login.

        Returns:
        JSON response containing the access token.
        """
        username = request.json.get("username", None)
        password = request.json.get("password", None)
        return jsonify(current_app.auth_service.login(username, password))

    @auth_bp.route("/loginTest", endpoint="/loginTest", methods=["GET"])
    def loginTest():
        """
        Endpoint for testing login with a predefined user.

        Returns:
        JSON response containing the access token.
        """
        access_token = create_access_token(
            identity="TEST", additional_claims={"profile": "base"}
        )
        return jsonify(access_token=access_token), 200
