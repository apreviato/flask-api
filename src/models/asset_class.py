from sqlalchemy import Column, Integer, String, Sequence
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class AssetClass(Base):
    __tablename__ = "AssetClass"
    
    id: int = Column(Integer, Sequence("asset_id_seq"), primary_key=True)
    name: str = Column(String(50))
    value: str = Column(String(50))
