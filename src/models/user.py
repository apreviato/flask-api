from enum import Enum
from sqlalchemy import Column, Integer, String, Sequence
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Profile(Enum):
    BASE = 0,
    AUDITOR = 1,
    MANAGER = 2,
    ADMIN = 3
    
class User(Base):
    __tablename__ = "user"

    id: int = Column(Integer, Sequence("user_id_seq"), primary_key=True)
    name: str = Column(String(50))
    username: str = Column(String(100))
    password: str = Column(String(100))
    profile: Profile = Column(Integer)

    def __init__(self, name: str, username: str, hash_password: str):
        self.name = name
        self.username = username
        self.password = hash_password
