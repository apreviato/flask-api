from models.asset_class import AssetClass
from sqlalchemy import Column, Integer, String, Sequence
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Project(Base):
    __tablename__ = "Project"
    
    id: int = Column(Integer, Sequence("project_id_seq"), primary_key=True)
    name: str = Column(String(50))
    classes: list[AssetClass] = []