class Config:
    SECRET_KEY = "###"
    DATABASE_URI = "sqlite:///db_tools.db"
    LOG_PATH = "app.log"
    BASE_URL = "/api/v1"
    DEBUG = False

class DevelopmentConfig(Config):
    DATABASE_URI = "sqlite:///db_tools_dev.db"
    DEBUG = True


class ProductionConfig(Config):
    SECRET_KEY = "###"
    DATABASE_URI = "postgresql://user:pass@localhost/database"


class TestingConfig(Config):
    TESTING = True
    DATABASE_URI = "sqlite:///:memory:"
